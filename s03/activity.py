import queue

class PriorityLane:
	def __init__(self):
		self.l = []

	def enqueue(self, item):
		self.l.insert(0,item)

	def dequeue(self):
		if len(self.l) == 0:
			print("No element present to remove!!")
			return None
		else:
			r = self.l.pop()
			return r

	def is_empty(self):
		if len(self.l) == 0:
			return True
		else:
			return False

	def peek(self):
		if len(self.l) == 0:
			print("No element present!!")
			return None
		else:
			return self.l[-1]

	def size(self):
		return len(self.l)

	def print(self):
		print("original queue list items:\n" + str(self.l) + '\n')

security = PriorityLane()

security.enqueue("senior")
security.enqueue("pregnant")
security.enqueue("PWD")
security.enqueue("normal person")
security.print()

empty = security.is_empty()
print(f'Is the security list empty? {empty}\n')

length = security.size()
print(f'The current size of the security list: {length}\n')

front_element = security.peek()
print(f'Peek of the new list queue item: {front_element}\n')

element_removed = security.dequeue()
print(f'The item is removed from the security list: {element_removed}\n')

if __name__ == "__main__":
	stack = queue.LifoQueue(maxsize=20)

	x = input()

	for i in x:
		if i != '(' and i != ')' and i != '+' and i != '-' and i != '/' and i != '*':
			print(i,end = "")
		elif i != ')':
			stack.put(i)
		else:
			x = stack.get()
			while x != '(':
				print(x,end = "")
				x = stack.get()