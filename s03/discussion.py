# [SECTION] Stack
# Functions associated with a stack:

# 1. empty()
stack = []

def empty(stack):
	if len(stack) == 0:
		print("The stack is empty")
	else:
		print("The stack are: {items}".format(items = stack))

empty(stack)

# 2. len() == size()
stack.append(5)
stack.append(6)
stack.append(10)

print(len(stack))

# 3. top() / peek()
def top(stack):
	if len(stack) == 0:
		print("Stack is empty")
	else:
		print(stack[-1])

top(stack)

# 4. push() / append()
stack.append(11)

print(stack)

# 5. pop()
stack.pop()

print(stack)


# [SECTION] Deque
from collections import deque

# Operations using deque:
# 1. deque()
stack_friends = deque(["Rachel", "Ross", "Joey", "Monica", "Chandler"])
print(stack_friends)


# 2. empty()
empty(stack_friends)

# 3. len()
print(len(stack_friends))

# 4. top()
top(stack_friends)

# 5. append() and pop()
stack_friends.append("Phoebe")
print(stack_friends)

stack_friends.pop()
print(stack_friends)

# 6. index(element, start, index)
print(stack_friends.index("Joey", 0, 4))

# 7. count() - Returns the amount of times an element occurs inside a stack
stack_friends.append("Rachel") # Adding another 'Rachel' to have 2 'Rachel's
print(stack_friends.count("Rachel"))

# 8. insert() - Allows us to append a new element on a specific index in the stack
stack_friends.insert(4, "Phoebe")
print(stack_friends)

# 9. rotate()
# If the number is positive, it will rotate from left to right
# If the number is negative, it will rotate from right to left
stack_friends.rotate(3) 
print(stack_friends)


# [SECTION] Queue
import queue

# Initializes a stack using the queue data structure
sample_students = queue.LifoQueue(maxsize=5)

# 1. empty() - returns a boolean depending on if the stack is empty or not.
print(sample_students.empty())

# 2. put() 
sample_students.put("Rick")
sample_students.put("Morty")
sample_students.put("Summer")
sample_students.put("Jerry")
sample_students.put("Beth")

print(sample_students.queue) # .queue allows us to get the contents of the queue

# 3. qsize() - returns the number of items in the queue
print(sample_students.qsize())

# 4. full() - returns a boolean value depending if the queue is full or not
print(sample_students.full())

# 5. maxsize - outputs the max size of the queue based on the initialization
print(sample_students.maxsize)