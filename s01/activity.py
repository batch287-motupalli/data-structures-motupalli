numbers = [1, 2, 3, 4, 5]
print(f'list of numbers: {numbers}\n')

meals = ['spam', 'Spam', 'SPAM!']
print(f'list of meals: {meals}\n')

messages = ['Hi!'] * 4
print(f'Result of repeat operator: {messages}\n')

print("List literals and operations using 'numbers' list:")
print(f'Length of the list: {len(numbers)}')
print(f'The last item was removed from the list: {numbers.pop()}')
numbers.reverse()
print(f'The list in the reverse order: {numbers}\n')

print("List literals and operations using 'meals' list:")
print(f'using indexing operation: {meals[2]}')
print(f'using negative indexing operation: {meals[-2]}')
meals[1] = 'eggs'
print(f'Modified list: {meals}')
meals.append('bacon')
print(f'Added a new item in the list: {meals}')
meals.sort()
print(f'Modified list after sort method: {meals}\n')

recipe = {
	'eggs' : 3	
}

print(f"Created recipe dictionary:\n{recipe}\n")
recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = "bacon"
print(f'Modified recipe dictionary:\n{recipe}\n')
recipe["ham"] = ['grill', 'bake', 'fry']
print(f"Updated the 'ham' key value:\n{recipe}\n")
del recipe["eggs"]
print(f"Modified recipe after deleting 'eggs' key:\n{recipe}\n")

bob = {
	'name' : {
		'first' : 'Bob',
		'last' : 'Smith'
	},
	'age' : 42,
	'job' : ['Software', 'writing'],
	'pay' : (40000, 50000)
}

print(f"Given 'bob' dictionary:\n{bob}\n")

print(f"Accessing the value of 'name' key: {bob['name']}")
print(f"Accessing the value of 'last' key: {bob['name']['last']}")
print(f"Accessing the second value of 'pay' key: {bob['pay'][1]}\n")

numeric = ('twelve', 3.0, [11, 22, 33])

print(f"Given 'numeric' dictionary:\n{numeric}\n")

print(f'Accessing a tuple item: {numeric[1]}')
print(f'Accessing a tuple item: {numeric[2][1]}')
