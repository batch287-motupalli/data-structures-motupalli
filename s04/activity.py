from numpy import *

rows = input("Enter the number of rows -> ")
columns = input("Enter the number of columns -> ")

print("Input the values for the first matrix")

y = []

for i in range(int(rows)):
	x = []
	for j in range(int(columns)):
		x.append(int(input(f'matrixOne[ {i} ][ {j} ] = ')))
	y.append(x)

matrixOne = array(y)

print("Input the values for the Second matrix")

y = []

for i in range(int(rows)):
	x = []
	for j in range(int(columns)):
		x.append(int(input(f'matrixTwo[ {i} ][ {j} ] = ')))
	y.append(x)

matrixTwo = array(y)

y = []

for i in range(int(rows)):
	x = []
	for j in range(int(columns)):
		x.append(matrixTwo[i][j] + matrixOne[i][j])
	y.append(x)

matrixSum = array(y)

print("*******Display Matrix1*******")
for i in range(int(rows)):
	for j in range(int(columns)):
		print(f'{matrixOne[i][j]}', end = "       ")
	print()

print("*******Display Matrix2*******")
for i in range(int(rows)):
	for j in range(int(columns)):
		print(f'{matrixOne[i][j]}', end = "       ")
	print()

print("Sum of Two matrix")
for i in range(int(rows)):
	for j in range(int(columns)):
		print(f'{matrixSum[i][j]}', end = "       ")
	print()