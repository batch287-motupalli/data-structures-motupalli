class Node:
	def __init__(self, color):
		self.color = color 
		self.next = None 

class ColorLinkedList:
	def __init__(self):
		self.head = None

	def prepend(self, color):
		new_node = Node(color) 
		new_node.next = self.head
		self.head = new_node 

	def append(self, color):
		new_node = Node(color)

		if self.head is None:
			self.head = new_node 
			return

		last_node = self.head 

		while last_node.next:
			last_node = last_node.next 

		last_node.next = new_node

	def print(self):
		current_node = self.head

		while current_node:
			print(current_node.color) 
			current_node = current_node.next

	def delete(self, key):
		current_node = self.head

		if current_node and current_node.color == key:
			self.head = current_node.next
			current_node = None
			return 

		previous = None 

		while current_node and current_node.color != key:
			previous = current_node 
			current_node = current_node.next

		if current_node is None:
			return 

		previous.next = current_node.next

		current_node = None

	def search(self, color):
		exist = False
		current_node = self.head

		while current_node:
			if current_node.color == color:
				exist = True
				break
			current_node = current_node.next

		return exist

	def count(self):
		count = 0

		current_node = self.head

		while current_node:
			count = count + 1
			current_node = current_node.next

		return count

llist = ColorLinkedList()

llist.append("Brown")
llist.append("Green")
llist.append("Blue")
llist.append("Grey")
llist.append("Black")

print("Original nodes of the linked list")
llist.print()
print("\n")

print("Insert the 'Purple' at the front of the linked list")
llist.prepend("Purple")
llist.print()
print("\n")

print("Delete the color 'Black' from the linked list")
llist.delete("Black")
llist.print()
print("\n")

grey = llist.search("Grey")
black = llist.search("Black")

if grey == True:
	grey = "Yes"
else: 
	grey = "No"

if black == True:
	black = "Yes"
else:
	black = "No"

print(f"Does the color 'Grey' exist in the linked list?\n{grey}\n")
print(f"Does the color 'Black' exist in the linked list?\n{black}\n")

print(f"Total number of nodes in the linked list:\n{llist.count()}")